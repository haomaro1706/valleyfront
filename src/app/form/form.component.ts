import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Menu } from '../interfaces/menu';
import { MenuService } from '../services/menu.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  menu: Menu = {
    nombre: null,
    estado: '1'
  };

  id: any;
  editar: boolean = false;
  menus: Menu[];
  constructor(private menuService: MenuService,private activateRoute: ActivatedRoute) { 
    this.id = this.activateRoute.snapshot.params['id'];
    if(this.id){
      this.editar = true;
      this.menuService.get().subscribe( (data: Menu[])=>{
        this.menus = data['data'];
        this.menu = this.menus.find( (m) => {return m.id == this.id} );
        console.log(this.menu);
      }, (error) =>{
        console.log(error);
      });
    }else{
      this.editar = false;
    }
  }

  ngOnInit(): void {
  }

  saveMenu(){
    if(this.editar){
      this.menuService.put(this.menu).subscribe( (data) => {
        alert('Datos actualizados');
        console.log(data);
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      });
    }else{
      this.menuService.save(this.menu).subscribe( (data) => {
        alert('Datos guardados');
        console.log(data);
      }, (error) => {
        console.log(error);
        alert('Ocurrio un error');
      });
    }
  }

}
