export interface Menu{
    id?: number;
    nombre: string;
    estado: string;
    created_at?: string;
    updated_at?: string;
}