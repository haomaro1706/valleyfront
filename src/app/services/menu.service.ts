import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Menu } from '../interfaces/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  API = 'http://localhost/valley/public/api/menus';
  constructor(private httpClient: HttpClient) { }

  get(){
    return this.httpClient.get( this.API );
  }

  save(menu: Menu){
    const headers = new HttpHeaders({'Content-type' : 'application/json'});
    return this.httpClient.post(this.API,menu,{headers:headers});
  }

  put(menu){
    const headers = new HttpHeaders({'Content-type' : 'application/json'});
    return this.httpClient.put(this.API + '/' + menu.id,menu,{headers:headers});
  }

  delete(id){
    return this.httpClient.delete(this.API + '/' + id);
  }

}
