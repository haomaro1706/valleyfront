import { Component, OnInit } from '@angular/core';
import { Menu } from '../interfaces/menu';
import {MenuService} from '../services/menu.service'; 

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  API = 'http://localhost/valley/public/api/menus';
  menus: Menu[];
  constructor(private menuService: MenuService) {
    this.getMenus();
   }

   getMenus(){
    this.menuService.get().subscribe( (data: Menu[]) =>{
      this.menus = data['data'];
    }, (error) =>{
      console.log(error);
      alert('Ocurrio un error');
    });
   }

  ngOnInit(): void {
  }


  delete(id){

    if(confirm("Seguro desea eliminar?")){
      this.menuService.delete(id).subscribe( (data) => {
        alert('Eliminado correctamente');
        console.log(data);
        this.getMenus();
      }, (error) => {
        console.log('error');
      } );
    }

  }
}
